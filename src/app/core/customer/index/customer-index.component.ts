import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment'
import { Customer } from '../customer.model';
import { CustomerService } from '../customer.service';
import * as $ from 'jquery';
import { ToastrManager } from 'ng6-toastr-notifications';


@Component({
  selector: 'customer-index',
  templateUrl: './customer-index.component.html',
  styleUrls: ['./customer-index.component.css']
})
export class CustomerIndexComponent implements OnInit {

  consumers: Customer[];
  consumer: Customer = new Customer();
  idDelete: string

  constructor(private customerService: CustomerService,public toastr: ToastrManager) { }

  ngOnInit() {
    this.findAllCustomer();

  }

  findAllCustomer() {
    this.customerService.findAll()
      .subscribe(
        response => {
          if (response.ok) {
            this.consumers = response.body;
          } else {
            this.toastr.errorToastr('Se produjo un error al obtener listado de clientes.', 'Error!');
            console.log(response);
          }
        },
        error => {
          this.toastr.errorToastr('Se produjo un error al obtener listado de clientes.', 'Error!');
          console.error(error);
        }
      );
  }



  updateCustomer(customerEdit: Customer) {
    this.consumer = customerEdit;
  }

  prepareDeleteCustomer(id: string) {
    this.idDelete = id;
  }

  prepareCreateCustomer() {
    this.consumer = new Customer();
    var alerta = document.getElementById("alerta2");
    if (alerta) {
      while (alerta.firstChild) {
        alerta.removeChild(alerta.firstChild);
      }
    }
  }



  deleteCustomer() {
    this.customerService.delete(this.idDelete)
      .subscribe(response => {
        if (response.ok) {
          this.consumers = response.body;
          this.findAllCustomer();
          document.getElementById('btnCloseModal').click();

          document.getElementById("alerta").innerHTML = `
          <div class="alert-success">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            Se eliminó cliente.
          </div>
           `;
          $("#alerta").fadeOut(5000, function () { });
        } else {

          document.getElementById("alerta").innerHTML = `
          <div class="alert-error">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            Se produjo un error al eliminar cliente.
          </div>`;
          $("#alerta").fadeOut(5000, function () { });
          console.log(response);
        }
      }, error => {
        document.getElementById("alerta").innerHTML = `
        <div class="alert-error">
          <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
          Se produjo un error al eliminar cliente.
        </div>`;
        $("#alerta").fadeOut(5000, function () { });
        console.log(error);
      });
  }


}
