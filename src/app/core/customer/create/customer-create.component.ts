import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Customer } from '../customer.model';
import { CustomerService } from '../customer.service';
import { ToastrManager } from 'ng6-toastr-notifications';



@Component({
  selector: 'customer-create',
  templateUrl: './customer-create.component.html',
  styleUrls: ['./customer-create.component.css']
})
export class CustomerCreateComponent implements OnInit {

  consumerEdit: Customer = new Customer();
  
  @Input() 
  consumer: Customer = new Customer();

  @Output()
  eventListCustomer = new EventEmitter<boolean>();


  constructor(private customerService: CustomerService,public toastr: ToastrManager) {

  

  }

  ngOnInit() { }

  


  createCustomer() {
    this.customerService.create(this.consumer)
      .subscribe((response) => {
        if (response.ok) {
          this.toastr.successToastr('This is success toast.', 'Success!');

          this.eventListCustomer.emit(null);

          setTimeout(function () {
              document.getElementById('alerta').innerHTML='';
          }, 5000);

           
        } else {
          this.toastr.errorToastr('Se produjo un error al registrar cliente.', 'Error!');
          console.log(response);
        }
      }, error => {
        console.log(error);
        this.toastr.errorToastr('Se produjo un error al registrar cliente.', 'Error!');

      })
  }

  updateCustomer() {
    this.customerService.update(this.consumer)
      .subscribe((response) => {
        if (response.ok) {
          document.getElementById('btnCloseCreateModal').click();
          document.getElementById("alerta").innerHTML = `
          <div class="alert-success">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            Se actualizaron los datos del cliente.
          </div>
           `;

           setTimeout(function () {
            document.getElementById('alerta').innerHTML='';
        }, 5000);

        } else {
          console.log(response);
          document.getElementById("alerta2").innerHTML = `
               <div class="alert-error">
                 <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                 <strong>Error!</strong> Se produjo un error al actualizar datos cliente.
               </div>`;

        }
      }, error => {
        console.log(error);
        document.getElementById("alerta2").innerHTML = `
        <div class="alert-error">
          <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
          <strong>Error!</strong>  Se produjo un error al actualizar datos cliente.
        </div>`;

      })

  }




}
