import { Injectable } from '@angular/core';
import { Customer } from './customer.model';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {


 BEARER:string="bearer ";

  constructor(private http:HttpClient) {


    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      })
    };
  }

  findAll():Observable<HttpResponse<Customer[]>>{


  
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'my-auth-token'
      }),
      observe:"response",
      responseType:'text' as 'text'
    };

     console.log("Consultando Clientes: Token= "+localStorage.getItem("token")) 
    return this.http.get<Customer[]>(environment.API_URL.concat('/customer')
    ,{headers:
        new HttpHeaders({'Content-Type': 'application/json',
                         'Authorization': this.BEARER+localStorage.getItem("token")}),
      observe: 'response'
    });
    
  }

  findById(id:string):Observable<HttpResponse<Customer>>{
    return this.http.get<Customer>(environment.API_URL.concat('/customer/'+id)
        ,{headers:
          new HttpHeaders({'Content-Type': 'application/json',
                           'Authorization': this.BEARER+localStorage.getItem("token")}),
        observe: 'response'
      });
  }


  create(customer:Customer):Observable<HttpResponse<any>>{
    return this.http.post(environment.API_URL.concat('/customer')
        ,customer
        ,{headers:
          new HttpHeaders({'Content-Type': 'application/json',
                           'Authorization': this.BEARER+localStorage.getItem("token")}),
        observe: 'response'
      });
  }

  delete(id:string):Observable<HttpResponse<any>>{
    return this.http.delete(environment.API_URL.concat('/customer/').concat(id) 
      ,{headers:
      new HttpHeaders({'Content-Type': 'application/json',
                       'Authorization': this.BEARER+localStorage.getItem("token")}),
      observe: 'response'
        }
    );
  }

  update(customer:Customer):Observable<HttpResponse<any>>{
   return  this.http.put(environment.API_URL.concat('/customer')
    ,customer 
    ,{headers:
      new HttpHeaders({'Content-Type': 'application/json',
                       'Authorization': this.BEARER+localStorage.getItem("token")}),
    observe: 'response'
  });
  }

}
