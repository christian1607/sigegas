import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CustomerComponent } from './customer/customer.component';
import { ProductComponent } from './product/product.component';
import { HomeComponent } from './home/home.component';
import { CustomerCreateComponent } from './customer/create/customer-create.component';
import { CustomerIndexComponent } from './customer/index/customer-index.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CoreComponent } from './core.component';
import { CreateComponent } from './product/create/create.component';
import { IndexComponent } from './product/index/index.component';
import { FileSelectDirective, FileUploadModule } from 'ng2-file-upload';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { DistributorComponent } from './distributor/distributor.component'
import { ToastrModule } from 'ng6-toastr-notifications';


@NgModule({
  imports: [
    
    CommonModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    FileUploadModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    RouterModule.forChild([
      {
        path:"core", component:CoreComponent,
          children:[
            { path:"customer", component:CustomerComponent},
            { path:"product", component:ProductComponent},
            { path:"distributor", component:DistributorComponent}
          ] 
      }
    ])
  ],
  declarations: [
    CoreComponent,
    ProductComponent,
    HomeComponent,
    CustomerComponent,
    CustomerCreateComponent,
    CustomerIndexComponent,
    CreateComponent,
    IndexComponent,
    
    DistributorComponent
    
  ],
  exports:[
    RouterModule,
    CoreComponent
  ]
})
export class CoreModule { }
