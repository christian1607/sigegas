import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from './product.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {




  BEARER: string = "bearer ";

  constructor(private http: HttpClient) {

  }

  findAll(): Observable<HttpResponse<Product[]>> {

    console.log("Obteniendo listado de productos")
    console.log(localStorage.getItem("token"));
    return this.http.get<Product[]>(environment.API_URL.concat('/product')
      , {
        headers:
          new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': this.BEARER + localStorage.getItem("token")
          }),
        observe: 'response'
      });

  }

  findById(id: string): Observable<HttpResponse<Product>> {
    return this.http.get<Product>(environment.API_URL.concat('/product/' + id)
      , {
        headers:
          new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': this.BEARER + localStorage.getItem("token")
          }),
        observe: 'response'
      });
  }


  create(customer: Product): Observable<HttpResponse<any>> {
    return this.http.post(environment.API_URL.concat('/product')
      , customer
      , {
        headers:
          new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': this.BEARER + localStorage.getItem("token")
          }),
        observe: 'response'
      });
  }

  delete(id: string): Observable<HttpResponse<any>> {
    return this.http.delete(environment.API_URL.concat('/product/').concat(id)
      , {
        headers:
          new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': this.BEARER + localStorage.getItem("token")
          }),
        observe: 'response'
      }
    );
  }

  update(customer: Product): Observable<HttpResponse<any>> {
    return this.http.put(environment.API_URL.concat('/product')
      , customer
      , {
        headers:
          new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': this.BEARER + localStorage.getItem("token")
          }),
        observe: 'response'
      });
  }

}
