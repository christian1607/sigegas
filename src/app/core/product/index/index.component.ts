import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Product } from '../product.model';
import { trigger, transition, style, animate } from '@angular/animations';
import * as $ from 'jquery';


@Component({
  selector: 'product-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  animations:[
    trigger('fade',[
        transition('*=>void',[
          animate(3000,style({opacity: 0}))  
        ])
    ])
  ]
})
export class IndexComponent implements OnInit {

  products: Product[];
  product: Product = new Product();
  idDelete: string

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.findAllProducts();

  }

  findAllProducts() {
    this.productService.findAll()
      .subscribe(
        response => {
          if (response.ok) {
            this.products = response.body;
          } else {
            document.getElementById("alerta").innerHTML = `
          <div class="alert-error">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            <strong>Error!</strong> Se produjo un error al obtener listado de productos.
          </div>
           `;
            console.log(response);
          }
        },
        error => {
          document.getElementById("alerta").innerHTML = `
          <div class="alert-error">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            <strong>Error!</strong> Se produjo un error al obtener listado de productos.
          </div>
           `;
          console.error(error);
        }
      );
  }



  updateCustomer(customerEdit: Product) {
    this.product = customerEdit;
  }

  prepareDeleteCustomer(id: string) {
    this.idDelete = id;
  }

  prepareCreateProduct() {
    this.product = new Product();
    var alerta = document.getElementById("alerta2");
    if (alerta) {
      while (alerta.firstChild) {
        alerta.removeChild(alerta.firstChild);
      }
    }

  }



  deleteProduct() {
    this.productService.delete(this.idDelete)
      .subscribe(response => {
        if (response.ok) {
          this.products = response.body;
          this.findAllProducts();
          document.getElementById('btnCloseModal').click();
          document.getElementById("alerta").innerHTML = `
          <div class="alert-success" @fade>
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            Producto eliminado.
          </div>
           `;

           $( "#alerta" ).fadeOut( 3000, function() {});

         
        } else {
          alert("Se produjo un error al eliminar cliente.")
          console.log(response);
        }
      }, error => {
        alert("Se produjo un error al eliminar cliente.")
        console.log(error);
      });
  }
}
