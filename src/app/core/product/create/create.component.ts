import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { ProductService } from '../product.service';
import { Product } from '../product.model';
import { FileUploader } from 'ng2-file-upload';

import { environment } from 'src/environments/environment';

@Component({
  selector: 'product-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {


  @Input() 
  product: Product = new Product();

  @Output()
  eventListProducts = new EventEmitter<boolean>();

  productEdit: Product = new Product();
  
  public uploader: FileUploader = new FileUploader({url: environment.API_URL.concat("/product/upload"),
   itemAlias: 'file',
   authToken: 'bearer '+ localStorage.getItem('token')
  });



  constructor(private productService:ProductService) { }

  ngOnInit() {

    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
         
         document.getElementById("alerta2").innerHTML = `
          <div class="alert-success">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            Se subio la imagen seleccionada.
          </div>
           `;
     };
  }

  createProduct() {
    this.productService.create(this.product)
      .subscribe((response) => {
        if (response.ok) {
          document.getElementById('btnCloseCreateModal').click();
          document.getElementById("alerta").innerHTML = `
          <div class="alert-success">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            Producto registrado satisfactoriamente.
          </div>
           `;

          this.eventListProducts.emit(null);

          setTimeout(function () {
              document.getElementById('alerta').innerHTML='';
          }, 5000);

           
        } else {
          document.getElementById("alerta").innerHTML = `
          <div class="alert-error">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            <strong>Error!</strong> Se produjo un error al registrar producto.
          </div>
           `;

          console.log(response);
        }
      }, error => {
        console.log(error);
        document.getElementById("alerta").innerHTML = `
          <div class="alert-error">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            <strong>Error!</strong> Se produjo un error al registrar producto.
          </div>`;

      })
  }

  updateProduct() {
    this.productService.update(this.product)
      .subscribe((response) => {
        if (response.ok) {
          document.getElementById('btnCloseCreateModal').click();
          document.getElementById("alerta").innerHTML = `
          <div class="alert-success">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
            Se actualizaron los datos del cliente.
          </div>
           `;

           setTimeout(function () {
            document.getElementById('alerta').innerHTML='';
        }, 5000);

        } else {
          console.log(response);
          document.getElementById("alerta2").innerHTML = `
               <div class="alert-error">
                 <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                 <strong>Error!</strong> Se produjo un error al actualizar datos del producto.
               </div>`;

        }
      }, error => {
        console.log(error);
        document.getElementById("alerta2").innerHTML = `
        <div class="alert-error">
          <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
          <strong>Error!</strong>  Se produjo un error al actualizar datos  del producto.
        </div>`;

      })

  }

}
