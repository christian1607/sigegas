import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { SecurityComponent } from './security.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      {
        path:"auth", component:LoginComponent,
          children:[
            { path:"login", component:LoginComponent,}
          ] 
      }
    ])
  ],
  declarations: [SecurityComponent,LoginComponent]
})
export class SecurityModule { }
