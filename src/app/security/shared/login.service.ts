import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "../../../environments/environment";
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs'
import { AppError } from "./appError.model";


@Injectable({
    providedIn: 'root'
  })
  export class LoginService {
  
  
  
    constructor(private http:HttpClient) {
    
    }
  
      
    login(username:string,password:string):Observable<HttpResponse<any>>{

       
     return  this.http.post(
        environment.API_URL_TOKEN.concat('token/generate-token'),
        { 
            username: username, 
            password: password 
        },{   
            observe: 'response'
        }).pipe(
            catchError((err:HttpErrorResponse)=>{
              
                console.log(err.error)
                return throwError(new AppError(err.error.timestampt,
                    err.error.message,
                    err.error.level
                    ));
                
            })
        );
    }
  
  }
  