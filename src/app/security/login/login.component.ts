import { Component, OnInit } from '@angular/core';
import { LoginService } from '../shared/login.service';
import { Token } from '../shared/token.model';
import {  Router }    from '@angular/router';
import { AppError } from '../shared/appError.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username:string="";
  password:string="";

  constructor(private loginService:LoginService,private router:Router) { }

  ngOnInit() {
  }

  login() {

    this.loginService.login(this.username,this.password)
      .subscribe(resp=>{
        
        if(resp.ok){
           var token:Token;
           token=resp.body;
           console.debug("Response: "+token.token);
           this.router.navigate(["/core/customer"]);
           localStorage.setItem("token",token.token); 

        }else{
          console.error("Se produjo un error.")
        }
      },(err:AppError)=>{
        console.log(err);
        
        document.getElementById("alert").innerHTML = `
        <div class="alert-error">
          <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
          `+err.message+`
        </div>
         `;
   
      })
      
  }

}
